package com.example.pr_idi.mydatabaseexample;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by howar on 2017-05-08.
 */

public class CoinAdapter extends ArrayAdapter<Coin> {
    public CoinAdapter(Context context, List<Coin> coins) {
        super(context, 0, coins);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        Coin coin = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = new View(getContext());
        }

        convertView = LayoutInflater.from(getContext()).inflate(R.layout.coinlistitem, parent, false);
        // Lookup view for data population
        TextView coinCurrency = (TextView) convertView.findViewById(R.id.currency);
        TextView coinCountry = (TextView) convertView.findViewById(R.id.country);
        TextView coinValue = (TextView) convertView.findViewById(R.id.value);
        // Populate the data into the template view using the data object
        coinCurrency.setText(coin.getCurrency());
        coinCountry.setText(coin.getCountry());
        coinValue.setText(Double.toString(coin.getValue()));

        ImageView imageView = (ImageView) convertView.findViewById(R.id.coin_image);
        ImageSaver imageSaver = new ImageSaver(getContext());
        imageSaver.setDirectoryName(Long.toString(coin.getId()));
        imageSaver.setFileName("0");
        Bitmap bitmap = imageSaver.load();
        if (bitmap == null) {
            imageSaver.setFileName("1");
            bitmap = imageSaver.load();
            if (bitmap != null) {
                imageView.setImageBitmap(bitmap);
            }
        }
        else {
            imageView.setImageBitmap(bitmap);
        }

        imageView.setScaleType(ImageView.ScaleType.FIT_XY);

        // Return the completed view to render on screen
        return convertView;
    }
}
