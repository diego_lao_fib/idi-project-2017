package com.example.pr_idi.mydatabaseexample;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.io.FileNotFoundException;
import java.io.InputStream;

public class AddCoinActivity extends AppCompatActivity {
    private CoinData coinData;
    private AndroidImageAdapter androidImageAdapter;
    private ViewPager viewPager;
    private int RESULT_LOAD_IMG = 109;
    private ImageSaver imageSaver;
    private Bitmap[] coinImages;

    public AddCoinActivity() {
        imageSaver = new ImageSaver(this);
        coinImages = new Bitmap[]{null, null};
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.coinadd);

        coinData = new CoinData(this);
        coinData.open();

        viewPager = (ViewPager) findViewById(R.id.imageslider);
        androidImageAdapter = new AndroidImageAdapter(this);
        viewPager.setAdapter(androidImageAdapter);
    }

    public void onClick(View view) {
//        @SuppressWarnings("unchecked")
        AlertDialog.Builder builder;
        switch (view.getId()) {
            case R.id.add_button:
                addImageToViewPager();
                break;
            case R.id.delete_button:
                deleteImageFromViewPager();
                break;
        }
    }

    private void addImageToViewPager() {
        if (androidImageAdapter.getCount() >= 2) {
            Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
            photoPickerIntent.setType("image/*");
            startActivityForResult(photoPickerIntent, RESULT_LOAD_IMG);
        } else {
            Toast.makeText(this, "No és possible ficar més de dos imatges", Toast.LENGTH_SHORT).show();
        }
    }

    private void deleteImageFromViewPager() {
        if (androidImageAdapter.getCount() >= 0) {
            androidImageAdapter.deleteImage(viewPager.getCurrentItem());
            androidImageAdapter.notifyDataSetChanged();
            coinImages[viewPager.getCurrentItem()] = null;
        } else {
            Toast.makeText(this, "No és possible eliminar una imatge que no existeix", Toast.LENGTH_SHORT).show();
        }
    }

    // Life cycle methods. Check whether it is necessary to reimplement them

    @Override
    protected void onResume() {
        coinData.open();
        super.onResume();
    }

    // Life cycle methods. Check whether it is necessary to reimplement them

    @Override
    protected void onPause() {
        coinData.close();
        super.onPause();
    }

    @Override
    protected void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            try {
                final Uri imageUri = data.getData();
                final InputStream imageStream = getContentResolver().openInputStream(imageUri);
                final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                int currentItemPosition = viewPager.getCurrentItem();
                androidImageAdapter.addImage(selectedImage, currentItemPosition);
                androidImageAdapter.notifyDataSetChanged();
                coinImages[currentItemPosition] = selectedImage;
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                Toast.makeText(AddCoinActivity.this, "Something went wrong", Toast.LENGTH_LONG).show();
            }

        } else {
            Toast.makeText(AddCoinActivity.this, "You haven't picked an image", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.addcoin, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        AlertDialog.Builder builder;
        switch (item.getItemId()) {
            case R.id.action_save:
                Toast.makeText(this, "Moneda creada!", Toast.LENGTH_SHORT).show();

                EditText currencyText = (EditText) findViewById(R.id.currency_field);
                String currency = currencyText.getText().toString();
                EditText valueText = (EditText) findViewById(R.id.value_field);
                Double value = Double.valueOf(valueText.getText().toString());
                EditText countryText = (EditText) findViewById(R.id.country_field);
                String country = countryText.getText().toString();
                EditText yearText = (EditText) findViewById(R.id.year_field);
                int year = Integer.valueOf(yearText.getText().toString());
                EditText descriptionText = (EditText) findViewById(R.id.description_field);
                String description = descriptionText.getText().toString();
                Coin coin = coinData.createCoin(currency, value, country, year, description);

                imageSaver.setDirectoryName(Long.toString(coin.getId()));

                if (coinImages[0] != null) {
                    imageSaver.setFileName("0");
                    imageSaver.save(coinImages[0]);
                }

                if (coinImages[1] != null) {
                    imageSaver.setFileName("1");
                    imageSaver.save(coinImages[1]);
                }

                finish();
                return true;
            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }
}
