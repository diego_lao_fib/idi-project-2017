package com.example.pr_idi.mydatabaseexample;


import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private CoinData coinData;
    private ListView listView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.coinlist);

        listView = (ListView) findViewById(android.R.id.list);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getApplicationContext(), CoinInformationActivity.class);
                Coin coin = (Coin) parent.getAdapter().getItem(position);
                intent.putExtra("COIN_ID", coin.getId());
                startActivity(intent);
            }
        });

        coinData = new CoinData(this);
        coinData.open();

        // use the SimpleCursorAdapter to show the
        // elements in a ListView
        List<Coin> values = coinData.getAllCoins();
        CoinAdapter adapter = new CoinAdapter(this, values);
        listView.setAdapter(adapter);

        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setMessage("¿Estás seguro de que quieres borrar esta moneda?");
                builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        CoinAdapter adapter = (CoinAdapter) listView.getAdapter();
                        Coin coin = adapter.getItem(position);
                        // Delete the coin from the list adapter
                        adapter.remove(coin);
                        adapter.notifyDataSetChanged();
                        // Delete the coin from the db
                        coinData.deleteCoin(coin);
                        ImageSaver imageSaver = new ImageSaver(getApplicationContext());
                        // Delete the coin images
                        imageSaver.setDirectoryName(Long.toString(coin.getId()));
                        imageSaver.setFileName("0");
                        imageSaver.delete();
                        imageSaver.setFileName("1");
                        imageSaver.delete();

                        return;
                    }
                });
                builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        return;
                    }
                });
                builder.show();
                return true;
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return true;
    }

    private void filterByCountryDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setView(getLayoutInflater().inflate(R.layout.custom_dialog, null));
        builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                EditText editText = (EditText) ((AlertDialog) dialog).findViewById(R.id.text_value);
                String country = editText.getText().toString();
                List<Coin> values = coinData.getAllCoins();
                List<Coin> valuesFiltered = new ArrayList<Coin>();

                for (int i = 0; i < values.size(); i++) {
                    Coin coinElement = values.get(i);
                    if (country.toLowerCase().equals(coinElement.getCountry().toLowerCase())) {
                        valuesFiltered.add(coinElement);
                    }
                }
                CoinAdapter coinAdapter = (CoinAdapter) listView.getAdapter();
                coinAdapter.clear();
                coinAdapter.addAll(valuesFiltered);
                coinAdapter.notifyDataSetChanged();
                return;
            }
        });
        builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                return;
            }
        });
        AlertDialog alertDialogDescription = builder.create();
        alertDialogDescription.show();
        EditText editText = (EditText) alertDialogDescription.findViewById(R.id.text_value);
        editText.setHint("Inserte el país");
    }

    private void filterByCurrencyDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setView(getLayoutInflater().inflate(R.layout.custom_dialog, null));
        builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                EditText editText = (EditText) ((AlertDialog) dialog).findViewById(R.id.text_value);
                String currency = editText.getText().toString();
                List<Coin> values = coinData.getAllCoins();
                List<Coin> valuesFiltered = new ArrayList<Coin>();

                for (int i = 0; i < values.size(); i++) {
                    Coin coinElement = values.get(i);
                    if (currency.toLowerCase().equals(coinElement.getCurrency().toLowerCase())) {
                        valuesFiltered.add(coinElement);
                    }
                }
                CoinAdapter coinAdapter = (CoinAdapter) listView.getAdapter();
                coinAdapter.clear();
                coinAdapter.addAll(valuesFiltered);
                coinAdapter.notifyDataSetChanged();
                return;
            }
        });
        builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                return;
            }
        });
        AlertDialog alertDialogDescription = builder.create();
        alertDialogDescription.show();
        EditText editText = (EditText) alertDialogDescription.findViewById(R.id.text_value);
        editText.setHint("Inserte la divisa");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        AlertDialog.Builder builder;
        switch (item.getItemId()) {
            case R.id.action_about:
                builder = new AlertDialog.Builder(MainActivity.this);
                builder.setMessage("Aplicación para la muestra de monedas. Para cualquier duda, existe el email aboutidi2017@gmail.com");
                builder.setNeutralButton("Entendido", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        return;
                    }
                });
                builder.show();
                return true;
            case R.id.action_help:
                builder = new AlertDialog.Builder(MainActivity.this);
                builder.setMessage("Se pueden añadir monedas pulsando el botón inferior derecho para posteriormente visualizar la información de la moneda introducida en la lista. Se pueden borrar las monedas desde la lista manteniendo pulsado la moneda que se quiere eliminar. Es posible editar las monedas seleccionando la moneda en cuestión de la lista y posteriormente editando los campos que se muestren.");
                builder.setNeutralButton("Entendido", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        return;
                    }
                });
                builder.show();
                return true;
            case R.id.action_filter:
                final CharSequence[] items = {"All", "Country", "Currency"};

                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(MainActivity.this);
                dialogBuilder.setTitle("Filter by:");
                dialogBuilder.setItems(items, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == 0) {
                            CoinAdapter coinAdapter = (CoinAdapter) listView.getAdapter();
                            coinAdapter.clear();
                            coinAdapter.addAll(coinData.getAllCoins());
                            coinAdapter.notifyDataSetChanged();
                        }
                        else if (which == 1) {
                            filterByCountryDialog();
                        }
                        else if (which == 2) {
                            filterByCurrencyDialog();
                        }
                    }
                });

                dialogBuilder.create().show();
                return true;

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }

    // Basic method to add pseudo-random list of books so that
    // you have an example of insertion and deletion

    // Will be called via the onClick attribute
    // of the buttons in main.xml
    public void onClick(View view) {
//        @SuppressWarnings("unchecked")
        switch (view.getId()) {
            case R.id.add_coin_fab:


//                String[] newCoins = new String[] { "Euro", "Dòlar", "Ien", "Lliura esterlina"};
//                double[] newValues = new double[] { 0.01, 0.05, 0.10, 0.50, 1 };
//                Coin coin;
//                ArrayAdapter<Coin> adapter = (ArrayAdapter<Coin>) ((ListView) findViewById(android.R.id.list)).getAdapter();
//                int c = new Random().nextInt(4);
//                int v = new Random().nextInt(5);
//
//                // save the new book to the database
//                if (c == 2) {
//                    coin = coinData.createCoin(newCoins[c], newValues[v]*100);
//                }
//                else {
//                    coin = coinData.createCoin(newCoins[c], newValues[v]);
//                }
//
//                // After I get the book data, I add it to the adapter
//                adapter.add(coin);
//                break;



                Intent intent = new Intent(this, AddCoinActivity.class);
                startActivity(intent);
                break;
        }
    }

    // Life cycle methods. Check whether it is necessary to reimplement them

    @Override
    protected void onResume() {
        coinData.open();
        List<Coin> values = coinData.getAllCoins();
        CoinAdapter adapter = new CoinAdapter(this, values);
        listView.setAdapter(adapter);
        super.onResume();
    }

    // Life cycle methods. Check whether it is necessary to reimplement them

    @Override
    protected void onPause() {
        coinData.close();
        super.onPause();
    }

}