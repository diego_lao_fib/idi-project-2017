package com.example.pr_idi.mydatabaseexample;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.FileNotFoundException;
import java.io.InputStream;

public class CoinInformationActivity extends AppCompatActivity {
    private Coin coin;
    private CoinData coinData;
    private AndroidImageAdapter androidImageAdapter;
    private ViewPager viewPager;
    private int RESULT_LOAD_IMG = 109;
    private ImageSaver imageSaver;

    public CoinInformationActivity() {
        imageSaver = new ImageSaver(this);
    }

    private void putIfThereArePreviousImages() {
        imageSaver.setDirectoryName(Long.toString(coin.getId()));
        imageSaver.setFileName("0");
        Bitmap bitmap1 = imageSaver.load();
        imageSaver.setFileName("1");
        Bitmap bitmap2 = imageSaver.load();
        androidImageAdapter.addImage(bitmap1, 0);
        androidImageAdapter.addImage(bitmap2, 1);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.coininformation);

        long coin_id = getIntent().getLongExtra("COIN_ID", -1);
        if (coin_id != -1) {
            coinData = new CoinData(this);
            coinData.open();
            coin = coinData.getCoin(coin_id);

            TextView coinValue = (TextView) findViewById(R.id.value);
            coinValue.setText(Double.toString(coin.getValue()));
            TextView coinCurrency = (TextView) findViewById(R.id.currency);
            coinCurrency.setText(coin.getCurrency());
            TextView coinCountry = (TextView) findViewById(R.id.country);
            coinCountry.setText(coin.getCountry());
            TextView coinYear = (TextView) findViewById(R.id.year);
            coinYear.setText(Integer.toString(coin.getYear()));
            TextView coinDescription = (TextView) findViewById(R.id.description);
            coinDescription.setText(coin.getDescription());

            viewPager = (ViewPager) findViewById(R.id.imageslider);
            androidImageAdapter = new AndroidImageAdapter(this);
            viewPager.setAdapter(androidImageAdapter);

            putIfThereArePreviousImages();

        }
    }

    public void onClick(View view) {
//        @SuppressWarnings("unchecked")
        AlertDialog.Builder builder;
        switch (view.getId()) {
            case R.id.add_button:
                addImageToViewPager();
                break;
            case R.id.delete_button:
                deleteImageFromViewPager();
                break;
            case R.id.edit_description_btn:
                builder = new AlertDialog.Builder(CoinInformationActivity.this);
                builder.setView(getLayoutInflater().inflate(R.layout.custom_dialog, null));
                builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        EditText editText = (EditText) ((AlertDialog) dialog).findViewById(R.id.text_value);
                        coin.setDescription(editText.getText().toString());
                        TextView descriptionTextView = (TextView) findViewById(R.id.description);
                        descriptionTextView.setText(editText.getText().toString());
                        Toast.makeText(CoinInformationActivity.this, "Cambio realizado", Toast.LENGTH_SHORT).show();
                        coinData.updateCoin(coin);
                        return;
                    }
                });
                builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        return;
                    }
                });
                AlertDialog alertDialogDescription = builder.create();
                alertDialogDescription.show();
                EditText editText = (EditText) alertDialogDescription.findViewById(R.id.text_value);
                editText.setHint("Inserte la nueva descripción");
                break;
            case R.id.edit_year_btn:
                builder = new AlertDialog.Builder(CoinInformationActivity.this);
                builder.setView(getLayoutInflater().inflate(R.layout.custom_dialog, null));
                builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        EditText editText = (EditText) ((AlertDialog) dialog).findViewById(R.id.text_value);
                        coin.setYear(Integer.valueOf(editText.getText().toString()));
                        TextView yearTextView = (TextView) findViewById(R.id.year);
                        yearTextView.setText(editText.getText().toString());
                        Toast.makeText(CoinInformationActivity.this, "Cambio realizado", Toast.LENGTH_SHORT).show();
                        coinData.updateCoin(coin);
                        return;
                    }
                });
                builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        return;
                    }
                });
                AlertDialog alertDialog = builder.create();
                alertDialog.show();
                EditText editTextDesc = (EditText) alertDialog.findViewById(R.id.text_value);
                editTextDesc.setInputType(InputType.TYPE_CLASS_NUMBER);
                editTextDesc.setHint("Inserte el nuevo año");
                break;
        }
    }

    private void addImageToViewPager() {
        if (androidImageAdapter.getCount() >= 2) {
            Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
            photoPickerIntent.setType("image/*");
            startActivityForResult(photoPickerIntent, RESULT_LOAD_IMG);
        }
        else {
            Toast.makeText(this, "No és possible ficar més de dos imatges", Toast.LENGTH_SHORT).show();
        }
    }

    private void deleteImageFromViewPager() {
        if (androidImageAdapter.getCount() >= 0) {
            androidImageAdapter.deleteImage(viewPager.getCurrentItem());
            androidImageAdapter.notifyDataSetChanged();

            imageSaver.setFileName(Integer.toString(viewPager.getCurrentItem()));
            imageSaver.delete();
        }
        else {
            Toast.makeText(this, "No és possible eliminar una imatge que no existeix", Toast.LENGTH_SHORT).show();
        }
    }

    // Life cycle methods. Check whether it is necessary to reimplement them

    @Override
    protected void onResume() {
        coinData.open();
        super.onResume();
    }

    // Life cycle methods. Check whether it is necessary to reimplement them

    @Override
    protected void onPause() {
        coinData.close();
        super.onPause();
    }

    @Override
    protected void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);


        if (resultCode == RESULT_OK) {
            try {
                final Uri imageUri = data.getData();
                final InputStream imageStream = getContentResolver().openInputStream(imageUri);
                final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                int currentItemPosition = viewPager.getCurrentItem();
                androidImageAdapter.addImage(selectedImage, currentItemPosition);
                androidImageAdapter.notifyDataSetChanged();
                imageSaver.setFileName(Integer.toString(currentItemPosition));
                imageSaver.save(selectedImage);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                Toast.makeText(CoinInformationActivity.this, "Something went wrong", Toast.LENGTH_LONG).show();
            }

        }else {
            Toast.makeText(CoinInformationActivity.this, "You haven't picked an image",Toast.LENGTH_LONG).show();
        }
    }
}
