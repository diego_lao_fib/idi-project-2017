package com.example.pr_idi.mydatabaseexample;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

/**
 * Created by howar on 2017-05-10.
 */

public class AndroidImageAdapter extends PagerAdapter {
    Context mContext;

    private Bitmap[] sliderImagesId;
    private Bitmap mImage;

    public AndroidImageAdapter(Context context) {
        this.mContext = context;
        sliderImagesId = new Bitmap[2];
    }

    public void addImage(Bitmap image, int pos) {
        sliderImagesId[pos] = image;
        mImage = image;
    }

    public void deleteImage(int pos) {
        sliderImagesId[pos] = null;
    }

    @Override
    public int getCount() {
        return sliderImagesId.length;
    }

    @Override
    public boolean isViewFromObject(View v, Object obj) {
        return v == ((ImageView) obj);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int i) {
        ImageView mImageView = new ImageView(mContext);
        mImageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        mImageView.setAdjustViewBounds(true);
        if (sliderImagesId[i] != null) mImageView.setImageBitmap(sliderImagesId[i]);
        ((ViewPager) container).addView(mImageView, 0);
        return mImageView;
    }

    @Override
    public void destroyItem(ViewGroup container, int i, Object obj) {
        ((ViewPager) container).removeView((ImageView) obj);
    }

    @Override
    public int getItemPosition(Object object) { return POSITION_NONE; }

}
